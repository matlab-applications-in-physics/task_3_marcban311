%MATLAB 2020b
%name:Task_3
%author:marcban311
%date: 23.11.2020
%version: v1.5

t = timer("ExecutionMode","fixedSpacing","Period",3600 ,"TimerFcn","M1");
t.TimerFcn= @(~,~)Task_3_v1;
start(t)

Task_3_v1();