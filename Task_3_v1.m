%MATLAB 2020b
%name:Task_3
%author:marcban311
%date: 27.11.2020
%version: v1.5

function weather_forecast()
station_1_URL= "https://wttr.in/kasprowy+wierch?format=j1";
station_2_URL= 'https://wttr.in/zakopane?format=j1';
station_3_URL= 'https://wttr.in/nowy+sacz?format=j1';

station_imgw_1_URL='https://danepubliczne.imgw.pl/api/data/synop/station/kasprowywierch?format=j1';
station_imgw_2_URL='https://danepubliczne.imgw.pl/api/data/synop/station/zakopane?format=j1';
station_imgw_3_URL='https://danepubliczne.imgw.pl/api/data/synop/station/nowysacz?format=j1';

station_imgw_1_meteoData= webread(station_imgw_1_URL);
station_imgw_2_meteoData= webread(station_imgw_2_URL);
station_imgw_3_meteoData= webread(station_imgw_3_URL);
station_1_meteoData= webread(station_1_URL);
station_2_meteoData= webread(station_2_URL);
station_3_meteoData= webread(station_3_URL);

time = clock ;
year = time(1);
month = time(2);
day = time(3);
hour = time(4);
minute= time(5);

sTemp_1 = 13.12 + 0.6215*str2num(station_imgw_1_meteoData.temperatura) - 11.37* str2num(station_imgw_1_meteoData.predkosc_wiatru)^(0.16) + 0.3965* str2num(station_imgw_1_meteoData.temperatura)* str2num(station_imgw_1_meteoData.predkosc_wiatru)^(0.16);
sTemp_2 = 13.12 + 0.6215*str2num(station_imgw_2_meteoData.temperatura) - 11.37* str2num(station_imgw_2_meteoData.predkosc_wiatru)^(0.16) + 0.3965* str2num(station_imgw_2_meteoData.temperatura)* str2num(station_imgw_2_meteoData.predkosc_wiatru)^(0.16);
sTemp_3 = 13.12 + 0.6215*str2num(station_imgw_3_meteoData.temperatura) - 11.37* str2num(station_imgw_3_meteoData.predkosc_wiatru)^(0.16) + 0.3965* str2num(station_imgw_3_meteoData.temperatura)* str2num(station_imgw_3_meteoData.predkosc_wiatru)^(0.16);

sunrise_1 = station_1_meteoData.weather(1).astronomy.sunrise;
sunset_1 = station_1_meteoData.weather(1).astronomy.sunset;
sunrise_2 = station_2_meteoData.weather(1).astronomy.sunrise;
sunset_2 = station_2_meteoData.weather(1).astronomy.sunset;
sunrise_3 = station_3_meteoData.weather(1).astronomy.sunrise;
sunset_3 = station_3_meteoData.weather(1).astronomy.sunset;

%i import weather data for each other location however for the kasprowy
%wierch there is no mesurement of presure 
format short g
imgw_1 = [year month day hour minute str2num(station_imgw_1_meteoData.temperatura) str2num(station_imgw_1_meteoData.predkosc_wiatru) str2num(station_imgw_1_meteoData.wilgotnosc_wzgledna) 0 0 ];
wttr_1 = [year month day hour minute str2num(station_1_meteoData.current_condition.temp_C) str2num(station_1_meteoData.current_condition.windspeedKmph)  str2num(station_1_meteoData.current_condition.humidity) str2num(station_1_meteoData.current_condition.pressure) sTemp_1];
imgw_2 = [year month day hour minute str2num(station_imgw_2_meteoData.temperatura) str2num(station_imgw_2_meteoData.predkosc_wiatru) str2num(station_imgw_2_meteoData.wilgotnosc_wzgledna) 0 0 ];
wttr_2 = [year month day hour minute str2num(station_2_meteoData.current_condition.temp_C) str2num(station_2_meteoData.current_condition.windspeedKmph)  str2num(station_2_meteoData.current_condition.humidity) str2num(station_2_meteoData.current_condition.pressure) sTemp_2];
imgw_3 = [year month day hour minute str2num(station_imgw_3_meteoData.temperatura) str2num(station_imgw_3_meteoData.predkosc_wiatru) str2num(station_imgw_3_meteoData.wilgotnosc_wzgledna) str2num(station_imgw_3_meteoData.cisnienie) 0 ];
wttr_3 = [year month day hour minute str2num(station_3_meteoData.current_condition.temp_C) str2num(station_3_meteoData.current_condition.windspeedKmph)  str2num(station_3_meteoData.current_condition.humidity) str2num(station_3_meteoData.current_condition.pressure) sTemp_3];

M1= [imgw_1; wttr_1; imgw_2; wttr_2; imgw_3; wttr_3];

%writematrix(M1 ,'M.csv','WriteMode','append');


rowNames = {'kasprowy_w_imgw';'kasprowy_w_wttrin';'zakopane_imgw';'zakopane_wttrin';'nowy_sacz_imgw';'nowy_sacz_wttrin'};
colNames = {'year','month','day','hour','minute','temp[°C]','wind_speed[kmph]','humidity[%]','air_presure[hPa]','sensed_temp[°C]'};
sM= array2table(M1,'RowNames',rowNames,'VariableNames',colNames)
qM= [rowNames sM];
writetable(qM, 'weatherData.csv','WriteMode','append');
%type('weatherData.csv');
%I also try to dave data as .xlsx for clear look in excel
writetable(qM,'weatherData.xlsx',"WriteMode","append","AutoFitWidth",true);

end 